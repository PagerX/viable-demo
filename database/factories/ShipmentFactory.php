<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Shipment;
use App\ShipmentUpdate;
use Faker\Generator as Faker;

$factory->define(Shipment::class, function (Faker $faker) {
    return [
        'delivery_date' => $faker->date(),
        'delivered' => 0,
        'tracking_code' => $faker->word(15)
    ];
});

$factory->define(ShipmentUpdate::class, function (Faker $faker) {
    return [
        'shipment_id' => factory('App\Shipment'),
        'update_message' => $faker->sentence()
    ];
});
