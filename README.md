# viable-demo

To setup the project please follow the next steps:

1)Open up a terminal in the project folder
2) Run composer install
3) Run npm install
4) Run php artisan migrate if you're going to use MySQL or SQLite as a database
5) Setup .env following the provided .env.example file*
6) Run npm run dev
7) Run php artisan serve
8) The app can now be accessed on the returned port

*DATASOURCE, DATASOURCE_PATH variable can be removed if not desired. Optional values for DATASROUCE are sqlite OR csv
Default SQLite path is ./database/database.sqlite
*tests can be run with the command vendor\bin\phpunit
