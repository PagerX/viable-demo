<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShipmentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Runs at the begining of each test
     * @return type
     */
    public function setUp() : void
    {
        parent::setup();
        $this->shipment = create('App\Shipment');
        $this->update = create('App\ShipmentUpdate', ['shipment_id' => $this->shipment->id]);
    }

    /** @test */
    public function we_can_retrieve_a_shipment_by_tracking_code()
    {
        $this->withoutExceptionHandling();
        $response = $this->post(
            '/api/shipments/track',
            ['tracking_code' => $this->shipment->tracking_code]
        )
        ->assertStatus(200);
        $shipment = (array) json_decode($response->content());
        $this->assertEquals($this->shipment->tracking_code, $shipment['tracking_code']);
    }

    /** @test */
    public function we_can_retrieve_a_shipment_from_csv()
    {
        //this test will only pass with the currently existing csv file
        //for production purposes we would probably mock a file via code to make sure it is there
        $this->shipment->tracking_code = 'abcdefg';
        Config::set('datasource.source', 'csv');

        $this->withoutExceptionHandling();
        $response = $this->post(
            '/api/shipments/track',
            ['tracking_code' => $this->shipment->tracking_code]
        )
        ->assertStatus(200);
        $shipment = (array) json_decode($response->content());
        $this->assertEquals($this->shipment->tracking_code, $shipment['tracking_code']);
    }
}
