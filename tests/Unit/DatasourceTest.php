<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatasourceTest extends TestCase
{
    /** @test */
    public function can_retrieve_data_source()
    {
        $source = Config::get('datasource.source');
        $this->assertEquals('sqlite', $source);
    }
}
