<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShipmentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Runs at the begining of each test
     * @return type
     */
    public function setUp() : void
    {
        parent::setup();
        $this->shipment = create('App\Shipment');
        $this->update = create('App\ShipmentUpdate', ['shipment_id' => $this->shipment->id]);
    }

    /** @test */
    public function a_shipment_has_updates()
    {
        $this->assertInstanceOf('App\ShipmentUpdate', $this->shipment->updates->first());
    }

    /** @test */
    public function a_shipment_update_belongs_to_a_shipment()
    {
        $this->assertInstanceOf('App\Shipment', $this->update->shipment);
    }
}
