<?php

/**
 * Creates a factory object which also saves it to the database
 * @param string $class
 * @param array $attributes
 * @return \Illuminate\Database\Eloquent\Factory
 */
function create(string $class, array $attributes = [])
{
    return factory($class)->create($attributes);
}

/**
 * Makes a factory object that is not yet saved to the database
 * please note, factory objects created with make, do not have an id
 * until actually saved to the database
 * @param string $class
 * @param array $attributes
 * @return \Illuminate\Database\Eloquent\Factory
 */
function make(string $class, array $attributes = [])
{
    return factory($class)->make($attributes);
}
