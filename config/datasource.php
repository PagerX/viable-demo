<?php

return [
    'source' => env('DATASOURCE' ,''),
    'csv' => [
        'path' => env('DATASOURCE_PATH', database_path('database.csv'))
    ]
];
