<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Respond with errors
     *
     * @param array|string $errors
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithError($status = 422, $errors)
    {
        return response()->json([
            'message' => $errors
        ], $status);
    }

    /**
     * Respond with success
     *
     * @param array $params
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithDone($params = [])
    {
        $data = ['data' => $params];
        $response = ['status' => 'success'];

        if (! empty($params)) {
            $response = array_merge($response, $data);
        }

        return response()->json($response);
    }

    /**
     * Respond with json
     *
     * @param $data
     * @param $key
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respond($data, $key = 'data', $multiple_keys = false, $response_code = 200)
    {
        if($multiple_keys) {
            return response()->json(
                $data, $response_code);
        }

        return response()->json([
            $key => $data
        ], $response_code);
    }
}
