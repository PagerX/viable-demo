<?php

namespace App\Http\Controllers;

use App\Shipment;
use Illuminate\Http\Request;

class ShipmentController extends Controller
{
    /**
     * Looks up a shipment by it's tracking code
     * @param Request $request
     * @return App\Shipment
     */
    public function track(Request $request)
    {
        if(!array_key_exists('tracking_code', $request->all()) || !$request['tracking_code']) {
            return $this->respondWithError(400, 'Tracking code missing from request. Should be under key "tracking_code"');
        }

        $shipment = new Shipment;

        $shipment = $shipment->findByCode($request['tracking_code']);

        if(!$shipment || !$shipment->tracking_code) {
            return $this->respondWithError(404, 'Shipment not found. Please make sure your tracking code is correct.');
        } else {
            return $shipment;
        }
    }
}
