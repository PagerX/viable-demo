<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $guarded = [];

    /**
     * HasMany relationship to ShipmentUpdate
     * @return Collection of App\ShipmentUpdate::class
     */
    public function updates()
    {
        return $this->hasMany(ShipmentUpdate::class, 'shipment_id', 'id');
    }

    /**
     * Can be used to find a shipment by tracking code,
     * based on our enviroment datasource set
     * @param type $tracking_code
     * @return Collection
     */
    public function findByCode($tracking_code)
    {
        $source = Config::get('datasource.source');

        if($source === 'sqlite') {
            //we switch db connection to sqlite if our datasource was set to sqlite
            return $this->on('sqlite')->where('tracking_code', $tracking_code)->with('updates')->first();
        } else if($source === 'csv') {
            //in case of csv, we'll just parse the file for the purpose of this demo
            $path = Config::get('datasource.csv.path');
            $shipments = array_map('str_getcsv', file($path));
            array_walk($shipments, function(&$a) use ($shipments) {
                $a = array_combine($shipments[0], $a);
            });
            array_shift($shipments);

            foreach ($shipments as $shipment) {
                if($shipment['tracking_code'] === $tracking_code) {
                    $this->tracking_code = $tracking_code;
                    $this->delivery_date = $shipment['delivery_date'];
                    $this->delivered = $shipment['delivered'];
                }
            }

            return $this;
        } else {
            //if no datasource is set in the app .env, we use the default database connection (usually mysql)
            return $this->where('tracking_code', $tracking_code)->with('updates')->first();
        }
    }
}
