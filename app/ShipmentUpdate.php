<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentUpdate extends Model
{
    protected $guarded = ['shipment_id'];

    /**
     * Belongsto  relationship to Shipment
     * @return App\Shipment::class
     */
    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }
}
